#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>

#define NBLIN 15
#define NBCOL 50
#define NBSTEPS 100

//intialization of the game support

void initArray(int nbLin,int nbCol,int iArray[nbLin][nbCol],int value)
{
    int i,j;
    //initialize the board with value
    for(i=0;i<nbLin;i ++)
    {
        for(j=0;j<nbCol;j ++)
        {
            iArray[i][j]=value;
        }
    }
}

void showArray(int nbLin,int nbCol,int iArray[nbLin][nbCol])
{
    int i,j;
    //show us the board
    for(i=0;i<nbLin;i ++)
    {
        for(j=0;j<nbCol;j ++)
        {
            printf("%d ",iArray[i][j]);
        }
        printf("\n");
    }
}

void copyArray(int nbLin, int nbCol, int array1[nbLin][nbCol], int array2[nbLin][nbCol])
{
    int i,j;
    for(i=0;i<nbLin;i ++)
    {
        for(j=0;j<nbCol;j ++)
        {
            array2[i][j]=array1[i][j];
        }
    }
}

//begining of life game

void seedRandomCells(int nbLin, int nbCol, int board[nbLin][nbCol])
{
    int i,j;
    //definition of the cell spawn probability
    for(i=0;i<nbLin;i ++)
    {
        for(j=0;j<nbCol;j ++)
        {
            if(rand()%5==0)
            {
                board[i][j]=1;
            }
            else
            {
                board[i][j]=0;
            }
        }
    }
}

int getNumberOfNeighboursAlive(int nbLin, int nbCol, int board[nbLin][nbCol], int cellLin, int cellCol)
{
    int numberOfNeighbours=0;
    //all corner cases
    if(cellLin==0 && cellCol==0)
    {
        numberOfNeighbours=board[cellLin+1][cellCol]+board[cellLin][cellCol+1]+board[cellLin+1][cellCol+1];
    }
    else if(cellLin==0 && cellCol==nbCol-1)
    {
        numberOfNeighbours=board[cellLin][cellCol-1]+board[cellLin+1][cellCol]+board[cellLin+1][cellCol-1];
    }
    else if(cellLin==nbLin-1 && cellCol==nbCol-1)
    {
        numberOfNeighbours=board[cellLin][cellCol-1]+board[cellLin-1][cellCol]+board[cellLin-1][cellCol-1];
    }
    else if(cellLin==nbLin-1 && cellCol==0)
    {
        numberOfNeighbours=board[cellLin][cellCol+1]+board[cellLin-1][cellCol]+board[cellLin-1][cellCol+1];
    }
    //all edge cases without corner cases
    else if((cellLin==0 && cellCol!=0)||(cellLin==0 && cellCol!=nbCol-1))
    {
        numberOfNeighbours=board[cellLin][cellCol-1]+board[cellLin][cellCol+1]+board[cellLin+1][cellCol-1]
        +board[cellLin+1][cellCol]+board[cellLin+1][cellCol+1];
    }
    else if((cellLin==nbLin-1 && cellCol!=0)||(cellLin==nbLin-1 && cellCol!=nbCol-1))
    {
        numberOfNeighbours=board[cellLin][cellCol-1]+board[cellLin][cellCol+1]+board[cellLin-1][cellCol-1]
        +board[cellLin-1][cellCol]+board[cellLin-1][cellCol+1];
    }
    else if((cellCol==0 && cellLin!=0)||(cellCol==0 && cellLin!=nbLin-1))
    {
        numberOfNeighbours=board[cellLin-1][cellCol]+board[cellLin-1][cellCol+1]+board[cellLin][cellCol+1]
        +board[cellLin+1][cellCol+1]+board[cellLin+1][cellCol];
    }
    else if((cellCol==nbCol-1 && cellLin!=0)||(cellCol==nbCol-1 && cellLin!=nbLin-1))
    {
        numberOfNeighbours=board[cellLin-1][cellCol]+board[cellLin-1][cellCol-1]+board[cellLin][cellCol-1]
        +board[cellLin+1][cellCol-1]+board[cellLin+1][cellCol];
    }
    //other cases
    else
    {
        numberOfNeighbours=board[cellLin-1][cellCol-1]+board[cellLin-1][cellCol]+board[cellLin-1][cellCol+1]
        +board[cellLin][cellCol+1]+board[cellLin+1][cellCol+1]+board[cellLin+1][cellCol]+board[cellLin+1][cellCol-1]
        +board[cellLin][cellCol-1];
    }
    return numberOfNeighbours;
}

int isCellDeadOrAlive(int nbLin, int nbCol, int board[nbLin][nbCol], int cellLin, int cellCol)
{
    int neighbours=getNumberOfNeighboursAlive(nbLin,nbCol,board,cellLin,cellCol);
    if((neighbours==2 || neighbours==3)&& (board[cellLin][cellCol]==1))
    {
        return 1;
    }
    else if(neighbours==3 && board[cellLin][cellCol]==0)
    {
        return 1;
    }
    else
    {
        return 0;
    }
}

int getNumberOfLivingCells(int nbLin,int nbCol,int board[nbLin][nbCol])
{
    int i,j;
    int NumberOfLivingCells=0;
    for(i=0;i<nbLin;i ++)
    {
        for(j=0;j<nbCol;j ++)
        {
            if(board[i][j]==1)
            {
                NumberOfLivingCells ++;
            }
        }
    }
    return NumberOfLivingCells;
}
// only for UBUNTU
//Function that clear the terminal's screen
/* Function that clear the terminal's screen */
void clearScreen() 
{
        /* Animation - clear the previous display */
        printf("%c[2J", 0x1B);
        /* Animation - Move the cursor top-left */
        printf("%c[%d;%dH", 0x1B, 1, 1);
}


void showBoard(int nbLin,int nbCol,int board[nbLin][nbCol])
{
    clearScreen();
    usleep(10000);
    int i,j;
    //show us the board
    for(i=0;i<nbLin;i ++)
    {
        for(j=0;j<nbCol;j ++)
        {
            if(board[i][j]==1)
            {
                printf("o");
            }
            else
            {
                printf(" ");
            }
        }
        printf("\n");
    }
}

void runAStep(int nbLin, int nbCol, int board[nbLin][nbCol])
{
    int i,j;
    int arraytmp[nbLin][nbCol];
    for(i=0;i<nbLin;i ++)
    {
        for(j=0;j<nbCol;j ++)
        {
            getNumberOfNeighboursAlive(nbLin,nbCol,board,i,j);
            arraytmp[i][j]=isCellDeadOrAlive(nbLin,nbCol,board,i,j);
        }
    }
    copyArray(nbLin,nbCol,arraytmp,board);
}

int runGame(int nbLin, int nbCol, int board[nbLin][nbCol])
{
    int i;
    int NumberOfLivingCells;
    seedRandomCells(nbLin,nbCol,board);//initialization of the board
    showBoard(nbLin,nbCol,board);
    //iterative loop for game
    for(i=0;i<NBSTEPS;i++)
    {
        runAStep(nbLin,nbCol,board);
        showBoard(nbLin,nbCol,board);//print the board
        NumberOfLivingCells=getNumberOfLivingCells(nbLin,nbCol,board);
        printf("\n\nIl y a %d cellules vivantes.\n",NumberOfLivingCells);
    }
    return 0;
}

int main()//int argc, char **argv)
{
    srand(time(NULL));
    int board[NBLIN][NBCOL];
    int value=0;
    initArray(NBLIN,NBCOL,board,value);
    //showArray(NBLIN,NBCOL,board);
    runGame(NBLIN,NBCOL,board);
    return 0;
}
