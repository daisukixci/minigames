#include <stdio.h>
#include <stdlib.h>

#define NBLIN 6
#define NBCOL 7

void initArray(int nbLin,int nbCol,int iArray[nbLin][nbCol],int value)
{
    int i,j;
    for(i=0;i<nbLin;i ++)
    {
        for(j=0;j<nbCol;j ++)
        {
            iArray[i][j]=value;
        }
    }
}

void showArray(int nbLin,int nbCol,int iArray[nbLin][nbCol])
{
    int i,j;
    for(i=0;i<nbLin;i ++)
    {
        for(j=0;j<nbCol;j ++)
        {
            printf("%d ",iArray[i][j]);
        }
        printf("\n");
    }
}

int getColumnForPawn(int nbLin,int nbCol,int board[nbLin][nbCol])
{
    int pawnCol;
    int boolean=0;
    printf("Enter the coordinate of the column you want to put your pawn on: ");
    while(boolean==0)
    {
        if (scanf("%d", &pawnCol)!=1)
        {
			exit(-1);
		}
        else if(pawnCol>=nbCol || pawnCol<0)
        {
            printf("Wrong number of column (outside of the board), please re-try.");
            printf("Enter the coordinate of the column you want to put your pawn on:");
        }
        else if(board[0][pawnCol]!=0)
        {
            printf("\nThis column is full so you can't add any more pawn, please re-try.\n");
            printf("Enter the coordinate of the column you want to put your pawn on:");
        }
        else
        {
            boolean=1;
        }
    }
    return pawnCol;
}

int placePawn(int nbLin,int nbCol,int board[nbLin][nbCol],int pawnCol,int pawn)
{
    int pawnLin=nbLin-1;
    while(board[pawnLin][pawnCol]!=0)
    {
        pawnLin --;
    }
    board[pawnLin][pawnCol]=pawn;
    return pawnLin;
}

int checkFourInLine(int nbLin,int nbCol,int board[nbLin][nbCol],int pawnLin,int pawnCol)
{
    int sumPawnInLine=0;
    int numPlayer=board[pawnLin][pawnCol];
    //horizontal test
    int boolean=0;
    int pawnColTmp=pawnCol+1,pawnLinTmp=pawnLin;
    while(boolean==0)
    {
        if(pawnColTmp>=nbCol)
        {
            boolean=1;
        }
        else if(board[pawnLinTmp][pawnColTmp]==numPlayer)
        {
            sumPawnInLine ++;
            pawnColTmp ++;
        }
        else
        {
            boolean=1;
        }
    }
    boolean=0;
    pawnColTmp=pawnCol-1;
    while(boolean==0)
    {
        if(pawnColTmp<0)
        {
            boolean=1;
        }
        else if(board[pawnLinTmp][pawnColTmp]==numPlayer)
        {
            sumPawnInLine ++;
            pawnColTmp --;
        }
        else
        {
            boolean=1;
        }
    }
    if(sumPawnInLine>=3)
    {
        return 1;
    }
    sumPawnInLine=0;
    //vertical test
    boolean=0;
    pawnColTmp=pawnCol;
    pawnLinTmp=pawnLin+1;
    while(boolean==0)
    {
        if(pawnLinTmp>=nbLin)
        {
            boolean=1;
        }
        else if(board[pawnLinTmp][pawnColTmp]==numPlayer)
        {
            sumPawnInLine ++;
            pawnLinTmp ++;
        }
        else
        {
            boolean=1;
        }
    }
    boolean=0;
    pawnLinTmp=pawnLin-1;
    while(boolean==0)
    {
        if(pawnLinTmp<0)
        {
            boolean=1;
        }
        else if(board[pawnLinTmp][pawnColTmp]==numPlayer)
        {
            sumPawnInLine ++;
            pawnLinTmp --;
        }
        else
        {
            boolean=1;
        }
    }
    if(sumPawnInLine>=3)
    {
        return 1;
    }
    sumPawnInLine=0;
    //diagonal up to down test
    boolean=0;
    pawnColTmp=pawnCol+1;
    pawnLinTmp=pawnLin+1;
    while(boolean==0)
    {
        if(pawnLinTmp>=nbLin || pawnColTmp>=nbCol)
        {
            boolean=1;
        }
        else if(board[pawnLinTmp][pawnColTmp]==numPlayer)
        {
            sumPawnInLine ++;
            pawnLinTmp ++;
            pawnColTmp ++;
        }
        else
        {
            boolean=1;
        }
    }
    boolean=0;
    pawnLinTmp=pawnLin-1;
    pawnColTmp=pawnCol-1;
    while(boolean==0)
    {
        if(pawnLinTmp<0 || pawnColTmp<0)
        {
            boolean=1;
        }
        else if(board[pawnLinTmp][pawnColTmp]==numPlayer)
        {
            sumPawnInLine ++;
            pawnLinTmp --;
            pawnColTmp --;
        }
        else
        {
            boolean=1;
        }
    }
    if(sumPawnInLine>=3)
    {
        return 1;
    }
    sumPawnInLine=0;
    //diagonal down to up test
    boolean=0;
    pawnColTmp=pawnCol+1;
    pawnLinTmp=pawnLin-1;
    while(boolean==0)
    {
        if(pawnLinTmp<0 || pawnColTmp>=nbCol)
        {
            boolean=1;
        }
        else if(board[pawnLinTmp][pawnColTmp]==numPlayer)
        {
            sumPawnInLine ++;
            pawnLinTmp --;
            pawnColTmp ++;
        }
        else
        {
            boolean=1;
        }
    }
    boolean=0;
    pawnLinTmp=pawnLin+1;
    pawnColTmp=pawnCol-1;
    while(boolean==0)
    {
        if(pawnLinTmp>=nbLin || pawnColTmp<0)
        {
            boolean=1;
        }
        else if(board[pawnLinTmp][pawnColTmp]==numPlayer)
        {
            sumPawnInLine ++;
            pawnLinTmp ++;
            pawnColTmp --;
        }
        else
        {
            boolean=1;
        }
    }
    if(sumPawnInLine>=3)
    {
        return 1;
    }
    return 0;
}

void clearScreen()
{
    /* Animation - clear the previous display*/
    printf("%c[2J", 0x1B);
    /* Animation - Move the cursor top-left*/
    printf("%c[%d;%dH", 0x1B, 1, 1);
}

void showBoard(int nbLin,int nbCol,int board[nbLin][nbCol])
{
    int i,j;
    clearScreen();
    for(i=0;i<nbLin;i ++)
    {
        for(j=0;j<nbCol;j ++)
        {
            printf(" | ");
            if(board[i][j]==0)
            {
                printf(" ");
            }
            else if(board[i][j]==1)
            {
                printf("\033[31;01mo \033[00m");
            }
            else if(board[i][j])
            {
                printf("\033[36;01mo \033[00m");
            }
        }
        printf("|");
        printf("\n");
    }
    printf(" ");
    for(i=0;i<nbCol;i++)
    {
        printf(" ---");
    }
    printf("\n");
    for(i=0;i<nbCol;i++)
    {
        printf("   %d",i);
    }
    printf("\n");
}

void endOfGame(int numWinner)
{
    printf("\n\n#### THE GAME IS OVER ####\n");
    if(numWinner==0)
    {
        printf("\nIt's a draw!\n");
    }
    else
    {
        printf("\nAnd the winner is Player %d!\n",numWinner);
    }
}

int runAStep(int nbLin,int nbCol,int board[nbLin][nbCol],int numPlayer)
{
    printf("\n#### Player %d, your turn ####\n",numPlayer);
    int pawnCol=getColumnForPawn(nbLin,nbCol,board);
    int pawnLin=placePawn(nbLin,nbCol,board,pawnCol,numPlayer);
    return checkFourInLine(nbLin,nbCol,board,pawnLin,pawnCol);
}

int runGame(int nbLin, int nbCol, int board[nbLin][nbCol])
{
    int booleen=1;
    int nbCoup=nbLin*nbCol-1;
    int pawn=1;
    int fourInLineCheck;
    showBoard(nbLin,nbCol,board);
    do
    {
        fourInLineCheck=runAStep(nbLin,nbCol,board,pawn);
        showBoard(nbLin,nbCol,board);
        if(nbCoup==0)
        {
            booleen=0;
        }
        nbCoup --;
        if(pawn==1)
        {
            pawn=2;
        }
        else
        {
            pawn=1;
        }
        if(fourInLineCheck==1)
        {
            booleen=0;
        }
    }
    while(booleen==1);
    if(pawn==1 && fourInLineCheck==1)
    {
        return 2;
    }
    else if(pawn==2 && fourInLineCheck==1)
    {
        return 1;
    }
    else
    {
        return 0;
    }
}


int main()//(int argc, char **argv)
{
    int board[NBLIN][NBCOL];
    int value=0;
    initArray(NBLIN,NBCOL,board,value);
    //showArray(NBLIN,NBCOL,board);
    int numWinner=runGame(NBLIN,NBCOL,board);
    endOfGame(numWinner);
    return 0;
}
